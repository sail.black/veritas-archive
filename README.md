# veritas.archive
[![Gitter](https://badges.gitter.im/sail-black/veritas-archive.svg)](https://gitter.im/sail-black/veritas-archive?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)


![veritas.archive](Logos/owl_distorted_small_gradient.jpg)

# Political landscape 

* ERC is interested and published a [document](https://erc.europa.eu/sites/default/files/document/file/ERC_info_document-Open_Research_Data_and_Data_Management_Plans.pdf)



The project is in ideation stage, but is mandatory for doing any further research. Science needs a real decentralized, open and accessible archive. It is needed urgently and so urgently that it seems to be worth more being able to organize what is already there than actually producing something new. 

Through the large political interest, it is no longer necessary to handle that by ourselves. Some people started to put more effort into this as the `Fel` (lets just call it like that) is destroying the very basis of our society. I know you think we might be joking but we are not joking. 

The only thing that makes us accelerate our own evolution so rapidly, is the preservation of knowledge. But the `Fel` is destroying that foundation of our society. Keep that in mind, the most important thing that makes  civilization great is the preservation of knowledge.

`For this, the openSuse Foundation puts know efforts into developing enterprise grade OSs that handle these problems. We hope the security with 2FA will be increased but thats also just a matter of time. We no longer continue to develop that project here but rather focus on security and AI, blockchain and foremost chemistry.`

# Why decentralized? 

It is clear that science is content-based. Decentralized data is content-based, too. So, both belong together.

# Preliminary thoughts on governance 

We want to build an open organization that hosts scientific (raw) data for everyone in a not-for-profit way. It seems important that there is a centralized platform, though, in the sense that there is only one platform for hosting scientific (raw) data. The platform must obey the values: 

1. Complete transparency while being immutable
2. Accessible
3. Protect authors and foster collaboration across borders

These values are lost more and more as the technology does not enable true scientists, but rather an elite of people abusing science for their selfish interests. We think it is because of culture and technology. Both influence each other significantly. And both lead to the state of low quality science and scientific data. Science must be the go to source for data again. Otherwise, hope for humanity is lost. 

Thus, we have to be political because our values are 
not shared with every government around the world. Therefore, we choose political sides. The same way, a scientist who is not political may not be useful to society or the culture they belong to. 

Science foremost belongs to humanity, and a scientist is working for the public. Selfish interests during the production of knowledge are entirely wrong and violate the deepest principles of science.

We see the problems of trying to build a fortune trying to use public knowledge and content (e.g. code). It could work better, and the resulting companies could be doing more for society, themselves, and their products. 

We need companies to run the economy, though. Thus, it makes still sense that `veritas.archive` is part of `sail.black`. As everything is open around `sail.black`, so is the veritas.archive. In case participants or the collective stop working in the setting of `sail.black` (whatever it will be), the acquired knowledge, and technology can't be lost. It will always exist. And thus, the space economy can be built on a decent knowledge foundation. 



# Shortlist of projects related

The section aims to collect technologies that might help building the `veritas.archive`. They are evaluated against the mission of `veristas.archive`

* [IPFS](https://ipfs.io/)
The general idea and need aligns well with the values of `IPFS`. What project could be more suitable? Offers lessons and tutorials on how to use the IPFS. Then, the US-Army is involved which makes it vulnerable to weird surveillance capitalism things. Avoid at all cost. 
* [Brave Browser](https://en.wikipedia.org/wiki/Brave_(web_browser))
Seems to be a good choice. IPFS not the way to go though. 
* [OrbitDB](https://github.com/orbitdb)
Unclear if it is a decent implementation, but the idea goes in the right direction. Not cooperative. 
* [Chemotion](https://github.com/ComPlat/chemotion_ELN)
A bit buggy but else a great project, great maintainers, should rather try to collaborate than to fork
Conclusion: Too complicated codebase, Ruby is not fitting into the stack. 
* [Chemotion_Alt](https://github.com/ComPlat/chemotion_REPO)
It seems to be old and abandoned (or only for chemists?). Also the Ruby and codebase problem. 
* [Zenodo](https://github.com/zenodo/zenodo)
Hosts tons of research. Is thus suitable for our needs (and is maintained.) Still, the NASA is involved which could lead to clashes with surveillance capitalism, though less likely than with the military directly. 
* [Haystack](https://haystack.deepset.ai/overview/intro)
Make use of the bulk data stored. Facebook uses it, and it can use multiple data sources 
* [Data Curator](https://github.com/qcif/data-curator)
Nice tool, could build something similar as an interface (Think MVP)
* [Ceph](https://ceph.io/en/)
Nice software, really nice. It is needed to host the stack in the end. Integrates with databases [Blog](https://www.burgundywall.com/post/postgres-ceph)
* [Silverblue](https://silverblue.fedoraproject.org/)
Nice OS suitable for the use case. Use containers to [deploy Ceph](https://docs.ceph.com/en/latest/cephadm/install/)

